#include "src/conditionnal_includes.h"

#include <M5Stack.h>
#include <arduino.h>
#include <Preferences.h>

#include "src/libraries/SystemHardware.h"
// #include "src/server/HttpServer.h"

#include "src/ui_compositions/DeviceStartingUIComposition.h"
#include "src/ui_compositions/MainUIComposition.h"

// wifi config store
Preferences preferences;
SystemHardware * system_hardware = new SystemHardware();
MainUIComposition * main_ui_composition = new MainUIComposition();
DeviceStartingUIComposition * device_starting_ui_composition = new DeviceStartingUIComposition();
// HttpServer * http_server = new HttpServer();

void setup() {

    main_ui_composition->setSystemHardware(system_hardware);
    device_starting_ui_composition->setSystemHardware(system_hardware);

    M5.begin();
    M5.Power.begin();
    Serial.begin(115200);

    // preferences.begin("wifi-configurations");
    
    // preferences.end();
    // String wifi_ssid = preferences.getString("WIFI_SSID");
    // String wifi_password = preferences.getString("WIFI_PASSWD");
    
    // WIFI_SSID, WIFI_PASSWORD

    // preferences.putString("WIFI_SSID", WIFI_SSID);
    // preferences.putString("WIFI_PASSWORD", WIFI_PASSWORD);

    system_hardware->loadWifiCredentials();

    Serial.println("Preferences WIFI_SSID: " + system_hardware->getWifiSSID());
    Serial.println("Preferences WIFI_PASSWORD: " + system_hardware->getWifiPassword());

    // delay(10000);

    device_starting_ui_composition->init();

    main_ui_composition->init();
}

void loop() {
    main_ui_composition->update();
}
