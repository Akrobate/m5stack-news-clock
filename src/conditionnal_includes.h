#ifndef ConditionnalIncludes_h
#define ConditionnalIncludes_h

#if __has_include("./images/custom_background_jpg.h")
#include "./images/custom_background_jpg.h"
#else
#include "./images/generic_background_jpg.h"
#endif

#if __has_include("./images/custom_start_screen_jpg.h")
#include "./images/custom_start_screen_jpg.h"
#else
#include "./images/generic_start_screen_jpg.h"
#endif

#if __has_include("./configurations/custom_configurations.h")
#include "./configurations/custom_configurations.h"
#else
#include "./configurations/configurations.h"
#endif

#endif