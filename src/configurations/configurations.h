
// Configurations Wifi
#define WIFI_SSID "YOUR_WIFI_SSID"
#define WIFI_PASSWORD "YOUR_WIFI_PASSWORD"

// Configurations Api
#define API_URL_NEWS "http://YOUR_URL:8090/api/news"
#define API_URL_BREAKING_NEWS "http://YOUR_URL:8090/api/breaking-news"

