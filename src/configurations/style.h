#ifndef style_h
#define style_h

#include <M5Stack.h>

#define COLOR_DARK_BLUE 19156
#define COLOR_CLOCK COLOR_DARK_BLUE // Dark blue
#define COLOR_BACKGROUND_BOTTOM_LINE COLOR_DARK_BLUE // Dark blue
#define COLOR_BACKGROUND_TOP_LINE TFT_RED // Red
#define COLOR_TEXT_BOTTOM_LINE TFT_WHITE // white
#define COLOR_TEXT_TOP_LINE TFT_WHITE // hhite

#define BRIGHTNESS 70

// const uint16_t COLOR_CLOCK = getFillColor(78, 89, 164); // Dark blue
// const uint16_t COLOR_BACKGROUND_BOTTOM_LINE = getFillColor(78, 89, 164); // Dark blue
// const uint16_t COLOR_BACKGROUND_TOP_LINE = getFillColor(255, 0, 0); // Red
// const uint16_t COLOR_TEXT_BOTTOM_LINE = getFillColor(255, 255, 255); // white
// const uint16_t COLOR_TEXT_TOP_LINE = getFillColor(255, 255, 255); // hhite

// uint16_t getFillColor(uint8_t r, uint8_t g, uint8_t b) {
//   return ((r / 8) << 11) | ((g / 4) << 5) | (b / 8);
// }

#endif
