#ifndef BatteryRenderer_h
#define BatteryRenderer_h

#include "AbstractRenderer.h"

#include <M5Stack.h>
#include "../configurations/style.h"

class BatteryRenderer: public AbstractRenderer
{
    public:

        TFT_eSprite * sprite;
        int level;
        BatteryRenderer();
        void setLevel(int level);
        void setCharging(bool is_charging);
        void setCharged(bool is_charged);
        void setIsCharging(bool is_charging);

    protected:

        void _draw();

    private:

        String text;
        bool is_charging;
        bool is_charged;

};

#endif