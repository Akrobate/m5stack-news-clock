#include "ScrollingTextLineRenderer.h"
 
ScrollingTextLineRenderer::ScrollingTextLineRenderer(SystemHardware * system_hardware) {

    this->sprite = new TFT_eSprite(system_hardware->lcd);
    this->sprite->createSprite(320, 29);
    
    this->refresh_interval_millis = 1;
    this->_position_top = 0;
    this->position_scroll_x = 325;
    this->_font_color = TFT_WHITE;
    this->_background_color = TFT_BLACK;
    this->_animation_step_px = 1;
}

void ScrollingTextLineRenderer::_draw() {

    if (this->text_width_px  < (this->position_scroll_x * -1)) {
        this->position_scroll_x = 325;
    }
    this->position_scroll_x = this->position_scroll_x - this->_animation_step_px;

    this->renderTextLineInSprite(
        this->text,
        this->position_scroll_x,
        this->_font_color,
        this->_background_color
    );
}

void ScrollingTextLineRenderer::_eraseDraw() {
    //this->sprite->fillScreen(TFT_WHITE);
    this->sprite->fillRect(0, 0, 320, 29, TFT_WHITE);
    this->sprite->pushSprite(0, this->_position_top);
}

void ScrollingTextLineRenderer::renderTextLineInSprite(String msg, int xpos, uint16_t text_color, uint16_t background_color) {
    this->sprite->fillRect(0, 0, 320, 29, background_color);
    this->sprite->setTextColor(text_color);
    this->sprite->setTextSize(1);
    this->sprite->setFreeFont(&CalibriNormal_24px);
    this->sprite->drawString(msg, xpos, 2);
    this->sprite->pushSprite(0, this->_position_top);
}

void ScrollingTextLineRenderer::setText(String text) {
    this->text = text;
    this->calculateTextWidthPx();
}

void ScrollingTextLineRenderer::calculateTextWidthPx() {
    M5.Lcd.setTextSize(1);
    M5.Lcd.setFreeFont(&CalibriNormal_24px);
    this->text_width_px = M5.Lcd.textWidth(this->text);
}

void ScrollingTextLineRenderer::setBackgroundColor(int color) {
    this->_background_color = color;
}

void ScrollingTextLineRenderer::setFontColor(int color) {
    this->_font_color = color;
}

void ScrollingTextLineRenderer::setAnimationStepWidthPx(int animation_step_px){
    this->_animation_step_px = animation_step_px;
}

void ScrollingTextLineRenderer::setPositionTop(int position_top) {
    this->_position_top = position_top;
}
