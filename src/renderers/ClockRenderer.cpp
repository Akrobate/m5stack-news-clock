#include "ClockRenderer.h"
 
ClockRenderer::ClockRenderer(SystemHardware * system_hardware) {
    this->sprite = new TFT_eSprite(system_hardware->lcd);
    this->sprite->createSprite(200, 70);
    this->refresh_interval_millis = 1000;
    this->last_time_millis = millis();
}

void ClockRenderer::_draw() {
    this->sprite->fillScreen(TFT_WHITE);
    this->sprite->setTextColor(COLOR_CLOCK, TFT_WHITE);
    this->sprite->setTextSize(2);
    this->sprite->setFreeFont(FSS18);
    this->sprite->drawString(this->text, 0, 0);
    this->sprite->pushSprite(55, 75);
}

void ClockRenderer::_eraseDraw() {
    this->sprite->fillScreen(TFT_WHITE);
    this->sprite->pushSprite(55, 75);
}

void ClockRenderer::setText(String text) {
    this->text = text;
}
