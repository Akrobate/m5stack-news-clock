#include "AbstractRenderer.h"

AbstractRenderer::AbstractRenderer() {
    this->_is_visible = true;
    this->last_time_millis = millis();
}

void AbstractRenderer::render() {
    if (
        this->_is_visible
        && ((millis() - this->last_time_millis) > this->refresh_interval_millis)
    ) {
        this->_draw();
        this->last_time_millis = millis();
    }
}

void AbstractRenderer::show() {
    this->setIsVisible(true);
}

void AbstractRenderer::hide() {
    this->_eraseDraw();
    this->setIsVisible(false);
}

void AbstractRenderer::setIsVisible(bool is_visible) {
    this->_is_visible = is_visible;
}

void AbstractRenderer::_eraseDraw() {}