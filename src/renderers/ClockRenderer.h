#ifndef ClockRenderer_h
#define ClockRenderer_h

#include "AbstractRenderer.h"

#include "../libraries/SystemHardware.h"

#include <M5Stack.h>
#include "../fonts/Free_Fonts.h"
#include "../configurations/style.h"

class ClockRenderer: public AbstractRenderer
{
    public:

        ClockRenderer(SystemHardware * system_hardware);
        void setText(String text);
        TFT_eSprite * sprite;

    protected:

        void _draw();
        void _eraseDraw();

    private:

        String text;

};

#endif