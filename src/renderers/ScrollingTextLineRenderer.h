#ifndef ScrollingTextLineRenderer_h
#define ScrollingTextLineRenderer_h

#include <M5Stack.h>
#include "AbstractRenderer.h"
#include "../fonts/Free_Fonts.h"
#include "../fonts/CalibriNormal_24px.h"
#include "../configurations/style.h"
#include "../libraries/SystemHardware.h"

class ScrollingTextLineRenderer: public AbstractRenderer
{
    public:

        TFT_eSprite * sprite;
        int position_scroll_x;
        int text_width_px;

        ScrollingTextLineRenderer(SystemHardware * system_hardware);
        void renderTextLineInSprite(String msg, int xpos, uint16_t text_color, uint16_t background_color);
        void calculateTextWidthPx();
        void setText(String text);
        void setBackgroundColor(int color);
        void setFontColor(int color);
        void setAnimationStepWidthPx(int step_px);
        void setPositionTop(int position_top);

    protected:

        void _draw();
        void _eraseDraw();

    private:

        String text;
        int _position_top;
        int _font_color;
        int _background_color;
        int _animation_step_px;

};

//    uint16_t bg[400] = {0};
//    M5.Lcd.readRect(160, 120, 20, 20, bg);
//    M5.Lcd.pushRect(0, 120, 20, 20, bg);

#endif