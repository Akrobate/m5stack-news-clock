#include "BatteryRenderer.h"
 
BatteryRenderer::BatteryRenderer() {
   this->sprite = new TFT_eSprite(&M5.Lcd);
   this->sprite->createSprite(22, 10);
    
    this->level = 0;
    this->refresh_interval_millis = 1000;
}

void BatteryRenderer::_draw() {
    this->sprite->fillScreen(TFT_WHITE);
    this->sprite->drawRect(0, 0, 19, 10, TFT_BLACK);
    this->sprite->drawRect(19, 2, 1, 6, TFT_BLACK);
    
    int color = COLOR_DARK_BLUE;

    if (this->is_charging) {
        color = TFT_ORANGE;
    }

    if (this->is_charged) {
        color = TFT_DARKGREEN;
    }
    
    for (int i = 0; i < this->level; i++) {
        if (level == 1) {
            color = TFT_RED;
        }
        this->sprite->fillRect(2 + (i * 4), 2, 3, 6, color);
    }
    
    this->sprite->pushSprite(296, 2);
}

void BatteryRenderer::setLevel(int level) {
    this->level = level;
}

void BatteryRenderer::setCharging(bool is_charging) {
    this->is_charging = is_charging;
}

void BatteryRenderer::setCharged(bool is_charged) {
    this->is_charged = is_charged;
}
