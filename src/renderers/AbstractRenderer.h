#ifndef AbstractRenderer_h
#define AbstractRenderer_h

#include <M5Stack.h>

class AbstractRenderer {

    public:
        AbstractRenderer();
        void render();
        void setIsVisible(bool is_visible);
        virtual void show();
        virtual void hide();

    protected:
        bool _is_visible;
        int refresh_interval_millis;
        int last_time_millis;

        virtual void _draw() = 0;
        virtual void _eraseDraw();

};

#endif