#include "HttpRouter.h"

HttpRouter::HttpRouter(WebServer * server) {
    this->server = server;

    this->settings_page_controller = new SettingsPageController(server);
    this->not_found_page_controller = new NotFoundPageController(server);
    this->home_page_controller = new HomePageController(server);
    this->initRoutes();
}

void HttpRouter::initRoutes() {
    this->server->on("/", [&]() {
        this->home_page_controller->process();
    });
    // Fix for home chrome
    this->server->on("/generate_204", [&]() {
        this->home_page_controller->process();
    });
    this->server->on("/settings", [&]() {
        this->settings_page_controller->process();
    });
    this->server->onNotFound([&]() {
        this->not_found_page_controller->process();
    });
}

void HttpRouter::setSystemHardware(SystemHardware * system_hardware) {
    this->system_hardware = system_hardware;
    this->settings_page_controller->setSystemHardware(this->system_hardware);
    this->not_found_page_controller->setSystemHardware(this->system_hardware);
    this->home_page_controller->setSystemHardware(this->system_hardware);
}