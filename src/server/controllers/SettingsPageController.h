#ifndef SettingsPageController_h
#define SettingsPageController_h

#include <M5Stack.h>
#include <WiFi.h>
#include <WiFiClient.h>
#include "../web_server/WebServer.h"
#include "AbstractController.h"

#include <arduino.h>

class SettingsPageController: public AbstractController {

    public:
        SettingsPageController(WebServer * server);
        void process();

        String getWifiCredentialForm();
        String getConfirmationPage();

};

#endif