#ifndef NotFoundPageController_h
#define NotFoundPageController_h

#include <M5Stack.h>
#include <WiFi.h>
#include <WiFiClient.h>
#include "../web_server/WebServer.h"
#include "AbstractController.h"

class NotFoundPageController: public AbstractController {

    public:
        NotFoundPageController(WebServer * server);
        void process();
};

#endif