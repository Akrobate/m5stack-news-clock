#include "HomePageController.h"

HomePageController::HomePageController(WebServer * server) {
    this->server = server;
}

void HomePageController::process() {
    String page_content = R"html(
<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width,user-scalable=0">
		<title>
			Home page
		</title>
		<style>
			body {
				font-family: sans-serif;
				margin-left: auto;
				margin-right: auto;
			}
		</style>
	</head>
	<body>
		<div style="border: 1px solid grey; margin: 20px; padding: 20px; border-radius: 20px; text-align: center">
			<h1 style="margin-top: 0px">Configurations</h1>
			<p>
                <a href="/settings">Wifi configuration</a>
            </p>
		</div>
	</body>
</html>
    )html";

    this->server->send(200, "text/html", page_content);
}
