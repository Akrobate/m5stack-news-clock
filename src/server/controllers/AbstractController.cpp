#include "AbstractController.h"

AbstractController::AbstractController() {
}

void AbstractController::setSystemHardware(SystemHardware * system_hardware) {
    this->system_hardware = system_hardware;
}

String AbstractController::urlDecode(String input) {
    String content = input;
    content.replace("%20", " ");
    content.replace("+", " ");
    content.replace("%21", "!");
    content.replace("%22", "\"");
    content.replace("%23", "#");
    content.replace("%24", "$");
    content.replace("%25", "%");
    content.replace("%26", "&");
    content.replace("%27", "\'");
    content.replace("%28", "(");
    content.replace("%29", ")");
    content.replace("%30", "*");
    content.replace("%31", "+");
    content.replace("%2C", ",");
    content.replace("%2E", ".");
    content.replace("%2F", "/");
    content.replace("%2C", ",");
    content.replace("%3A", ":");
    content.replace("%3A", ";");
    content.replace("%3C", "<");
    content.replace("%3D", "=");
    content.replace("%3E", ">");
    content.replace("%3F", "?");
    content.replace("%40", "@");
    content.replace("%5B", "[");
    content.replace("%5C", "\\");
    content.replace("%5D", "]");
    content.replace("%5E", "^");
    content.replace("%5F", "-");
    content.replace("%60", "`");
    return content;
}