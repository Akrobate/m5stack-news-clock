#ifndef HomePageController_h
#define HomePageController_h

#include <M5Stack.h>
#include <WiFi.h>
#include <WiFiClient.h>
#include "../web_server/WebServer.h"
#include "AbstractController.h"

class HomePageController: public AbstractController {

    public:
        HomePageController(WebServer * server);
        void process();
};

#endif