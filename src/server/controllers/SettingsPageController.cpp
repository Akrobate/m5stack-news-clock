#include "SettingsPageController.h"


SettingsPageController::SettingsPageController(WebServer * server) {
    this->server = server;
}


void SettingsPageController::process() {

	String page_content = "";
	if (this->server->hasArg("ssid")) {
		Serial.println("Credentials send");

		String ssid = this->urlDecode(this->server->arg("ssid"));
		String password = this->urlDecode(this->server->arg("password"));

		Serial.println(ssid);
		Serial.println(password);

		this->system_hardware->saveWifiSSID(ssid);
		this->system_hardware->saveWifiPassword(password);

		page_content = this->getConfirmationPage();

	} else {

    	page_content = this->getWifiCredentialForm();

	}
    this->server->send(200, "text/html", page_content);

}


String SettingsPageController::getWifiCredentialForm() {

    String page_content = R"html(
<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width,user-scalable=0">
		<title>

		</title>
		<style>
			body {
				font-family: sans-serif;
				margin-left: auto;
				margin-right: auto;
			}
		</style>
	</head>
	<body>
		<div style="border: 1px solid grey; margin: 20px; padding: 20px; border-radius: 20px; text-align: center">
			<h1 style="margin-top: 0px">Wi-Fi Settings</h1>
			<p>Please enter your wifi network name and your wifi password</p>
			<form method="get">
				<p>
					Wifi network: <br />
					<input name="ssid" type="text">
				</p>
				<p>
					Password: <br />
					<input name="pass" type="password">
				</p>
				<input type="submit" value="save" style="padding: 7px 20px 7px 20px; border-radius: 10px;">
			</form>
		</div>
	</body>
</html>
    )html";

    return page_content;
}


String SettingsPageController::getConfirmationPage() {

    String page_content = R"html(
<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width,user-scalable=0">
		<title>

		</title>
		<style>
			body {
				font-family: sans-serif;
				margin-left: auto;
				margin-right: auto;
			}
		</style>
	</head>
	<body>
		<div style="border: 1px solid grey; margin: 20px; padding: 20px; border-radius: 20px; text-align: center">
			<h1 style="margin-top: 0px">Wi-Fi Settings</h1>

			<p>Your wifi credentials has been saved</p>

		</div>
	</body>
</html>
    )html";

    return page_content;
}


