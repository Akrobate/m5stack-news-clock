#include "NotFoundPageController.h"

NotFoundPageController::NotFoundPageController(WebServer * server) {
    this->server = server;
}

void NotFoundPageController::process() {
    String page_content = R"html(
        <!DOCTYPE html>
        <html>
            <head>
                <meta name="viewport" content="width=device-width,user-scalable=0">
                <title>
                    Not found page
                </title>
            </head>
            <body>
                <h1>Not found page - AP mode</h1><p><a href="/settings">Wi-Fi Settings</a></p>
            </body>
        </html>
    )html";

    this->server->send(200, "text/html", page_content);
}
