#ifndef AbstractController_h
#define AbstractController_h

#include <M5Stack.h>
#include "../web_server/WebServer.h"
#include "../../libraries/SystemHardware.h"

class AbstractController {

    public:
        AbstractController();
        virtual void process() = 0;
        void setSystemHardware(SystemHardware * system_hardware);

    protected:
        WebServer * server;
        SystemHardware * system_hardware;
        String urlDecode(String input);
};

#endif