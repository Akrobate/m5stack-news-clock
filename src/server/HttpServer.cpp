#include "HttpServer.h"

HttpServer::HttpServer() {
    this->access_point_ssid = "M5IotClock";
    this->access_point_password = "";
    this->port = 80;
    this->dns_port = 53;
    this->access_point_ip = new IPAddress(8,8,4,4);
    this->access_point_subnet = new IPAddress(255, 255, 255, 0);
}

void HttpServer::initServer() {
    this->dns_server = new DNSServer();
    this->server = new WebServer(this->port);
    this->http_router = new HttpRouter(this->server);
    this->http_router->setSystemHardware(this->system_hardware);
}


void HttpServer::initWifiAccessPoint() {
    WiFi.softAP(this->access_point_ssid.c_str());
    WiFi.softAPConfig(
        *this->access_point_ip,
        *this->access_point_ip,
        *this->access_point_subnet
    );
    WiFi.mode(WIFI_MODE_AP);
    delay(500);
}

void HttpServer::startWebServer() {
    this->is_running = true;
    Serial.println("startWebServer - WiFi.softAPIP()");
    Serial.println(WiFi.softAPIP());
    this->http_router->initRoutes();
    this->dns_server->start((const byte)this->dns_port, "*", *this->access_point_ip);
    this->server->begin();
}


void HttpServer::handleClient() {
    this->dns_server->processNextRequest();
    this->server->handleClient();
}

bool HttpServer::isRunning() {
    return this->is_running;
}

void HttpServer::stopWebServer() {
    this->is_running = false;
}

void HttpServer::setSystemHardware(SystemHardware * system_hardware) {
    this->system_hardware = system_hardware;
}