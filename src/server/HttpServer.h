#ifndef HttpServer_h
#define HttpServer_h

#include <M5Stack.h>
#include <WiFi.h>
#include <WiFiClient.h>
#include "web_server/WebServer.h"
#include "HttpRouter.h"
#include "../libraries/SystemHardware.h"
#include <DNSServer.h>
// DNS Captive portal configuration
// https://github.com/espressif/arduino-esp32/blob/master/libraries/DNSServer/examples/CaptivePortal/CaptivePortal.ino
// https://randomnerdtutorials.com/esp32-access-point-ap-web-server/
// https://thekurks.net/blog/2017/11/6/esp8266-iot-eeprom-example

class HttpServer {

    public:
        String access_point_ssid;
        String access_point_password;
        int port;
        int dns_port;
        WebServer * server;
        DNSServer * dns_server;
        HttpRouter * http_router;
        
        IPAddress * access_point_ip;
        IPAddress * access_point_subnet;
        
        bool is_running;

        SystemHardware * system_hardware;

        HttpServer();
        void initServer();
        void startWebServer();
        void initWifiAccessPoint();

        void handleClient();

        bool isRunning();
        void stopWebServer();

        void setSystemHardware(SystemHardware * system_hardware);

};

#endif
