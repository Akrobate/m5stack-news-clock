#ifndef HttpRouter_h
#define HttpRouter_h

#include <M5Stack.h>
#include <WiFi.h>
#include <WiFiClient.h>
#include "web_server/WebServer.h"
#include "controllers/HomePageController.h"
#include "controllers/SettingsPageController.h"
#include "controllers/NotFoundPageController.h"
#include "../libraries/SystemHardware.h"

class HttpRouter {

    public:
        WebServer * server;
        SystemHardware * system_hardware;

        HomePageController * home_page_controller;
        SettingsPageController * settings_page_controller;
        NotFoundPageController * not_found_page_controller;

        HttpRouter(WebServer * server);
        void initRoutes();

        void setSystemHardware(SystemHardware * system_hardware);

};

#endif