#ifndef SystemHardware_h
#define SystemHardware_h

#include <M5Stack.h>
#include <Preferences.h>

#include "../conditionnal_includes.h"

class SystemHardware {

    public:
        SystemHardware();
        M5Display * lcd;
        Preferences * preferences;

        String wifi_ssid;
        String wifi_password;

        bool wifi_connect_failed;
        bool wifi_connected;

        bool web_server_is_running;
        
        void connectWifi();
        void loadWifiCredentials();
        void saveWifiSSID(String ssid);
        void saveWifiPassword(String password);

        void setWifiSSID(String ssid);
        void setWifiPassword(String password);

        String getWifiSSID();
        String getWifiPassword();

        void setWebServerIsRunning(bool is_running);
        bool getWebServerIsRunning();
};
#endif