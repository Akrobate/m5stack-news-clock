#include "SystemHardware.h"
#include <WiFi.h>

SystemHardware::SystemHardware() {
    this->lcd = &M5.Lcd;
    this->wifi_connect_failed = false;
    this->wifi_connected = false;
    this->preferences = new Preferences();

}

void SystemHardware::loadWifiCredentials() {
    this->wifi_ssid = preferences->getString("WIFI_SSID", "");
    this->wifi_password = preferences->getString("WIFI_PASSWORD", "");
}

void SystemHardware::saveWifiSSID(String ssid) {
    this->preferences->putString("WIFI_SSID", ssid);
}

String SystemHardware::getWifiSSID() {
    return this->wifi_ssid;
}

void SystemHardware::setWifiSSID(String ssid) {
    this->wifi_ssid = ssid;
}

void SystemHardware::saveWifiPassword(String password) {
    this->preferences->putString("WIFI_PASSWORD", password);
}

String SystemHardware::getWifiPassword() {
    return this->wifi_password;
}

void SystemHardware::setWifiPassword(String password) {
    this->wifi_password = password;
}

void SystemHardware::connectWifi() {
    WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.println("Connecting to WiFi..");
    }
}

void SystemHardware::setWebServerIsRunning(bool is_running) {
    this->web_server_is_running = is_running;
}

bool SystemHardware::getWebServerIsRunning() {
    return this->web_server_is_running;
}
