#include "NewsLoader.h"

NewsLoader::NewsLoader(String news_url) {
    this->news_url = news_url;
}

int NewsLoader::loadNewsData() {
    return this->_loadNewsData(
        this->text_array,
        this->text_array_count,
        this->news_url
    );
}

int NewsLoader::_loadNewsData(String text_array[], int text_array_size, String news_url) {
    int load_count = 0;
    String payload;
    HTTPClient http;
    http.begin(news_url);
    int httpCode = http.GET();
    if (httpCode > 0) {
        payload = http.getString();
        Serial.println(httpCode);
        Serial.println(payload);

        // Deserialize the JSON document
        DeserializationError error = deserializeJson(doc, payload.c_str());

        // Test if parsing succeeds.
        if (error) {
            Serial.print(F("deserializeJson() failed: "));
            Serial.println(error.f_str());
        } else {
            int content_size = doc.size();
            // Serial.println("Content size: " + String(content_size));
            load_count = content_size;

            for(int i = 0; i < content_size; i++) {
                text_array[i] = doc[i]["content"].as<String>();
                Serial.println(doc[i]["content"].as<String>());
            }
        }
    } else {
        Serial.println("Error on HTTP request");
    }
    http.end(); //Free the resources
    return load_count;
}

String NewsLoader::getTextLine() {
    return this->_concatenateStringArray(
        this->text_array,
        this->text_array_count
    );
}

String NewsLoader::_concatenateStringArray(String array[], int size) {
    String result = "";
    for (int i = 0; i < size; i++) {
        result.concat(array[i]);
        result.concat("            ");
    }
    return result;
}

