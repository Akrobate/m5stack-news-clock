#ifndef NewsLoader_h
#define NewsLoader_h

#include <arduino.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>

class NewsLoader
{
    public:
        String news_url;
        const int text_array_count = 10;
        String text_array[10] = {
            "server offline",
            "server offline",
            "server offline",
            "server offline",
            "server offline",
            "server offline",
            "server offline",
            "server offline",
            "server offline",
            "server offline"
        };
        StaticJsonDocument<5000> doc;
        NewsLoader(String news_url);
        int loadNewsData();
        String getTextLine();

    private:
        int _loadNewsData(String text_array[], int text_array_size, String news_url);
        String _concatenateStringArray(String array[], int size);
};

#endif