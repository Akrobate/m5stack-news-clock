#include "DateTimeData.h"

DateTimeData::DateTimeData(){
}

String DateTimeData::getTimeString() {
    struct tm timeinfo;
    if(!getLocalTime(&timeinfo)){
        Serial.println("Failed to obtain time");
        return "";
    }
    Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
    char time_string[9];
    sprintf(time_string, " %02d:%02d ", timeinfo.tm_hour, timeinfo.tm_min);
    // sprintf(time_string, "%02d:%02d:%02d", tm_hour, tm_min, tm_sec); // With secs version
    return String(time_string);
}

void DateTimeData::config() {
    configTime(
        this->gmtOffset_sec,
        this->daylightOffset_sec,
        this->ntpServer
    );
}