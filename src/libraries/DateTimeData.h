#ifndef DateTimeData_h
#define DateTimeData_h

#include <arduino.h>
#include "time.h"

class DateTimeData
{
    public:
        const char* ntpServer = "pool.ntp.org";
        const long  gmtOffset_sec = 3600;
        const int   daylightOffset_sec = 3600;

        DateTimeData();
        void config();
        String getTimeString();
    private:


};

#endif