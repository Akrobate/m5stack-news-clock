#include "DeviceStartingUIComposition.h"

DeviceStartingUIComposition::DeviceStartingUIComposition() {
    this->http_server = new HttpServer();
}

void DeviceStartingUIComposition::init() {

    // PUT DEVICE in config mode
    if (false) {
        this->http_server->setSystemHardware(this->system_hardware);
        this->http_server->initServer();
        this->http_server->initWifiAccessPoint();
        this->http_server->startWebServer();
        while(this->http_server->isRunning()) {
            this->http_server->handleClient();
        }
    }

    this->system_hardware->lcd->setBrightness(BRIGHTNESS);
    this->system_hardware->lcd->fillScreen(BLACK);
    this->system_hardware->lcd->drawJpg(start_screen_jpg, START_SCREEN_JPG_SIZE);

    this->system_hardware->lcd->setTextColor(TFT_WHITE, COLOR_BACKGROUND_BOTTOM_LINE);
    this->system_hardware->lcd->setTextSize(1);
    this->system_hardware->lcd->setFreeFont(FSS12);
    this->system_hardware->lcd->drawString("Starting...", 30, 150);
    delay(1500);

    Serial.println("Initing connection");
    this->system_hardware->lcd->drawString("Wifi connectiong...", 30, 150);
    this->system_hardware->connectWifi();
    this->system_hardware->lcd->drawString("Connected               ", 30, 150);
    delay(1000);
    this->system_hardware->lcd->drawString("Happy business!", 30, 150);
    delay(1500);
}

void DeviceStartingUIComposition::update() {}

void DeviceStartingUIComposition::unload() {}
