#ifndef SetupUIComposition_h
#define SetupUIComposition_h

#include "AbstractUIComposition.h"
#include <arduino.h>
#include <WiFi.h>
#include "../fonts/Free_Fonts.h"
#include "../configurations/style.h"

class SetupUIComposition: public AbstractUIComposition {

    public:
        SetupUIComposition();

        void update();
        void init();
        void unload();
};

#endif