#include "MainUIComposition.h"

MainUIComposition::MainUIComposition() {
}

void MainUIComposition::init() {

    this->news_loaded = false;

    this->news_loader = new NewsLoader(API_URL_NEWS);
    this->breaking_news_loader = new NewsLoader(API_URL_BREAKING_NEWS);
    this->date_time_data = new DateTimeData();

    this->clock_renderer = new ClockRenderer(system_hardware);
    this->top_line_renderer = new ScrollingTextLineRenderer(system_hardware);
    this->bottom_line_renderer = new ScrollingTextLineRenderer(system_hardware);
    this->battery_renderer = new BatteryRenderer();


    this->date_time_data->config();

    this->system_hardware->lcd->fillScreen(WHITE);     // CLEAR SCREEN
    this->system_hardware->lcd->drawJpg(background_jpg, BACKGROUND_JPG_SIZE);

    this->top_line_renderer->setPositionTop(14);
    this->top_line_renderer->setBackgroundColor(COLOR_BACKGROUND_TOP_LINE);
    this->top_line_renderer->setAnimationStepWidthPx(2);
    this->bottom_line_renderer->setBackgroundColor(COLOR_BACKGROUND_BOTTOM_LINE);
    this->bottom_line_renderer->setPositionTop(203);

    this->battery_renderer->setLevel(1);
}

void MainUIComposition::update() {

    M5.update();
    
    if (!this->news_loaded) {
        this->news_loader->loadNewsData();
        this->breaking_news_loader->loadNewsData();
        this->top_line_renderer->setText(breaking_news_loader->getTextLine());
        this->bottom_line_renderer->setText(news_loader->getTextLine());
        this->news_loaded = true;
    }

    this->battery_renderer->setLevel(M5.Power.getBatteryLevel() / 25);
    this->battery_renderer->setCharged(M5.Power.isChargeFull());
    this->battery_renderer->setCharging(M5.Power.isCharging());
    this->battery_renderer->render();

    this->clock_renderer->setText(date_time_data->getTimeString());
    this->clock_renderer->render();
    this->top_line_renderer->render();
    this->bottom_line_renderer->render();

    if (M5.BtnA.wasPressed()) {
        this->top_line_renderer->show();
        this->clock_renderer->show();
    }
    if (M5.BtnB.wasPressed()) {
        this->clock_renderer->hide();
    }
    if (M5.BtnC.wasPressed()) {
        this->top_line_renderer->hide();
    }
}

void MainUIComposition::unload() {
}
