#ifndef AbstractUIComposition_h
#define AbstractUIComposition_h

#include "../libraries/SystemHardware.h"

class AbstractUIComposition {

    public:
        SystemHardware * system_hardware;
        AbstractUIComposition();
        virtual void update() = 0;
        void setSystemHardware(SystemHardware * system_hardware);
};

#endif