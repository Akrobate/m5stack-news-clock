#ifndef MainUIComposition_h
#define MainUIComposition_h

#include "./AbstractUIComposition.h"

// @TODO: Try to remove this
#include <M5Stack.h>

#include "../conditionnal_includes.h"
#include "../configurations/style.h"

#include "../libraries/NewsLoader.h"
#include "../libraries/DateTimeData.h"
#include "../renderers/ClockRenderer.h"
#include "../renderers/ScrollingTextLineRenderer.h"
#include "../renderers/BatteryRenderer.h"

#include "../fonts/Free_Fonts.h"
#include "../fonts/CalibriNormal_24px.h"

class MainUIComposition: public AbstractUIComposition {

    public:
        MainUIComposition();
        ~MainUIComposition();

        void update();
        void init();
        void unload();

    
    private:

        bool news_loaded;

        NewsLoader * news_loader;
        NewsLoader * breaking_news_loader;
        DateTimeData * date_time_data;

        ClockRenderer * clock_renderer;
        ScrollingTextLineRenderer * top_line_renderer;
        ScrollingTextLineRenderer * bottom_line_renderer;
        BatteryRenderer * battery_renderer;

};

#endif