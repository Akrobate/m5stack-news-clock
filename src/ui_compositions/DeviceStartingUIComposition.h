#ifndef DeviceStartingUIComposition_h
#define DeviceStartingUIComposition_h

#include "AbstractUIComposition.h"
#include <arduino.h>
#include <WiFi.h>
#include "../server/HttpServer.h"
#include "../fonts/Free_Fonts.h"
#include "../configurations/style.h"

class DeviceStartingUIComposition: public AbstractUIComposition {

    public:
        DeviceStartingUIComposition();
        HttpServer * http_server;

        void update();
        void init();
        void unload();
};

#endif