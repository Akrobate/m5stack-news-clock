# m5stack-news-clock

Everything can be customized in this project

```sh
# Each of this files can be copied with custom_ prefix
./src/images/generic_background_jpg.h
./src/images/generic_start_screen_jpg.h
./src/configurations/configurations.h

# When this files exists they will replace the inclusion of generic files
./src/images/custom_background_jpg.h
./src/images/custom_start_screen_jpg.h
./src/configurations/custom_configurations.h

```